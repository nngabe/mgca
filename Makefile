CC = gcc-8
TARGET = mgca_MP
CFLAGS = -O3 -fopenmp -lm -DN=16000

default: 
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c

clean:$
	rm $(TARGET)
