#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h>

//#define N 200
#define M 2800


#define ran() ( (double) (rand() / (double)RAND_MAX))  //generates a random number [0,1]

#define ran_r(s) ( (double)rand_r(&s)/(double)RAND_MAX  )

#define rand_int(i,j) ( ( i<=j  ) ? (int)(i + (j-i+1)*ran())  : (int)(j + (i-j+1)*ran())  )

#define sgn(i) ( (i>0) ? 1 : (i<0) ? -1  : 0  )

#define mod(j,q) ( ((j % q) + q) % q  ) // modular arithematic needed for toroidal boundary conditions
typedef struct{

	double cap; // capital
	int a; // position on stock
	int active; // 0 if not active 1 if active

} agent;

int count_active(agent *agent, int NM); // count number of active agents
int moore_neighbors(agent *agent,int k); // count how man neighbors an agent has
void move_rand(agent *agent, int k); // move to a random space in moore neighborhood


int main(int argc, char** argv){

  int max_steps = atoi(argv[1]);
	agent *agt = (agent*)  malloc(N*M*sizeof(agent));
	int i,j,k;

	int A = 0;
  int N_neigh = 0;
	double p = 200.;
	double p_new = 0.;
	double dp = 0.;
	double lam = N*M/log(1.2);
	double r = 0.;
	
	// int seed = time(NULL)*time(NULL)%100
	int seed = 19;	

	srand(seed); ran();

	for(i=0;i<N*M;i++){
		r = ran();
		agt[i].a = sgn(r-.5);
		// printf("agt[%i]=%i, rand()=%f\n",i,agt[i].a,r);
		agt[i].active = sgn(rand_int(0,4));
		if ( agt[i].active==1){
			agt[i].cap = 100;
		}
		else{
			agt[i].cap = 0;
            agt[i].a = 0;
		}
	}
	int count = count_active(agt,N*M);

	for(j=0; j<M; j++){
		for(i=0;i<N;i++){
			//printf("%+i ",agt[i+j*N].a*agt[i+j*N].active);
            A += agt[i+j*N].a*agt[i+j*N].active;
		}
		//printf("\n");
	}
    printf("A = %+i\n",A);
    printf("active_agents = %+i\n",count);
    int t;
	for(t=0;t<max_steps;t++){
        if (p<10.){
            p=10.;
        }
		A=0.;
		#pragma omp parallel for private(k) reduction(+:A)	
		for(k=0;k<M*N;k++){ 
    	A+= agt[k].a*agt[k].active;
		}
    p_new = p*exp(A/lam);
		dp = p_new-p;
		p = p_new;
        //printf("A = %+i\n",A);
        //printf("dp = %+f\n",dp);
		#pragma omp parallel for private(j) 
		for(j=0; j<M; j++){
			for(i=0;i<N;i++){
	            if(agt[i+j*N].a == -1*agt[(i+1)%N + j*N].a ){
                    //printf("a[%i][%i],a[%i][%i] = %+i,%+i\n",j,i,j,(i+1)%N,agt[i+j*N].a, agt[(i+1)%N + j*N].a);
                    agt[i+j*N].cap -=  agt[i+j*N].a*dp;
                    agt[(i+1)%N + j*N].cap += agt[i+j*N].a*dp;
                }
                if(agt[i+j*N].a == -1*agt[(i+1)%N + ((j+1)%M)*N].a ){
                    //printf("a[%i][%i],a[%i][%i] = %+i,%+i\n",j,i,(j+1)%N,(i+1)%N,agt[i+j*N].a, agt[(i+1)%N + ((j+1)%N)*N].a);
                    agt[i+j*N].cap -=  agt[i+j*N].a*dp;
                    agt[(i+1)%N + ((j+1)%M)*N].cap += agt[i+j*N].a*dp;
                }	
                if(agt[i+j*N].a == -1*agt[i + ((j+1)%M)*N].a ){
                    //printf("a[%i][%i],a[%i][%i] = %+i,%+i\n",j,i,(j+1)%N,i,agt[i+j*N].a, agt[i + ((j+1)%N)*N].a);
                    agt[i+j*N].cap -=  agt[i+j*N].a*dp;
                    agt[i + ((j+1)%M)*N].cap += agt[i+j*N].a*dp;
                }	
                if(agt[i+j*N].a == -1*agt[(i+1)%N + mod(j-1,M)*N].a ){
                    //printf("a[%i][%i],a[%i][%i] = %+i,%+i\n",j,i,mod(j-1,N)%N,(i+1)%N,agt[i+j*N].a, agt[(i+1)%N + mod(j-1,N)*N].a);
                    agt[i+j*N].cap -=  agt[i+j*N].a*dp;
                    agt[(i+1)%N + mod(j-1,M)*N].cap += agt[i+j*N].a*dp;
                }	


			}
		}
        // remove agents in debt and update position of active agents
 		#pragma omp parallel private(k,seed)
		{
			unsigned int seed = omp_get_thread_num();
			#pragma omp for
			for(k=0;k<M*N;k++){ 
      	if(agt[k].cap< 0.){
          		agt[k].a = 0;
              agt[k].active = 0;
              agt[k].cap = 0.;
              //printf("agent[%i] removed\n",k);
            
        }
        else if(agt[k].cap>0.){
          		r = (double) rand_r(&seed)/(double)RAND_MAX;
              agt[k].a = sgn(r-.5);
      	}
			}
		}
	}  

	/*for(j=0; j<M; j++){
		for(i=0;i<N;i++){
			if (agt[i+j*N].cap>0.)
				printf("%+.4f ",agt[i+j*N].cap);
		}
		printf("\n");
	}*/
  printf("num_agents = %i\n",count);  
	printf("N = %i\n",N);
	printf("M = %i\n",M);


    free(agt);
	return 0;
}

int count_active(agent *agent, int NM){

	int i;
	int count=0;

	for(i=0;i<NM;i++) count +=agent[i].active;

	return count;
}

int moore_neighbors(agent *agent, int k){
    int m,n;
    int res=0;
    int i = mod(k,N);
    int j = (int) k/N;

    for(m=-1;m<=1;m++){
        for(n=-1;n<=1;n++){
            if ( n!=0 || m!=0){
                res += agent[mod(i+m,N) + mod(j+n,M)*N].active;
                if (agent[mod(i+m,N) + mod(j+n,M)*N].active){
                    //printf("i,j = %i,%i ...active\n",mod(i+m,N),mod(j+n,M));
                }
            }
        }
    }
    return res;
}

void move_rand(agent *agent, int k){
    int x,y;
    int i = mod(k,N);
    int j = (int) k/N;
    double r;
    r = ran();
    x = sgn(r-.5);
    r = ran();
    y = sgn(r-.5);
    //printf("(%i,%i) -> (%i,%i)\n",i,j,mod(i+x,N),mod(j+y,M));

    agent[mod(i+x,N) + mod(j+y,M)*N].cap = agent[i+j*N].cap;
    agent[mod(i+x,N) + mod(j+y,M)*N].a = agent[i+j*N].a;
    agent[mod(i+x,N) + mod(j+y,M)*N].active = agent[i+j*N].active;
    agent[i+j*N].cap = 0.;
    agent[i+j*N].a = 0;
    agent[i+j*N].active = 0;

}

